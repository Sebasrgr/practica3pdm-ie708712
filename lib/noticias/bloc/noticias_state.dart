part of 'noticias_bloc.dart';

abstract class NoticiasState extends Equatable {
  const NoticiasState();

  @override
  List<Object> get props => [];
}

class NoticiasInitial extends NoticiasState {}

class NoticiasSuccessState extends NoticiasState {
  final List<Noticia> noticiasSportList;
  final List<Noticia> noticiasBusinessList;

  NoticiasSuccessState(
      {@required this.noticiasBusinessList, @required this.noticiasSportList});
  @override
  List<Object> get props => [noticiasBusinessList, noticiasSportList];
}

class NoticiasErrorState extends NoticiasState {
  final String error;

  NoticiasErrorState({@required this.error});
  @override
  List<Object> get props => [];
}

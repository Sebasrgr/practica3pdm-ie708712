import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noticias/mis_noticias/bloc/mis_noticias_bloc.dart';

class CrearNoticia extends StatefulWidget {
  CrearNoticia({Key key}) : super(key: key);

  @override
  _CrearNoticiaState createState() => _CrearNoticiaState();
}

class _CrearNoticiaState extends State<CrearNoticia> {
  var tituloController = TextEditingController();
  var descriController = TextEditingController();
  MisNoticiasBloc sharedBloc;

  @override
  void dispose() {
    sharedBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) {
          sharedBloc = MisNoticiasBloc();
          return sharedBloc;
        },
        child: BlocConsumer<MisNoticiasBloc, MisNoticiasState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            return Center(
              child: Form(
                child: Column(
                  children: [
                    SizedBox(
                      height: 28,
                    ),
                    OutlineButton(
                      onPressed: () {
                        sharedBloc.add(
                            CargarImagenEvent(takePictureFromCamera: true));
                      },
                      child: Text("Cargar imagen"),
                    ),
                    SizedBox(
                      height: 28,
                    ),
                    TextFormField(
                      controller: tituloController,
                      decoration: InputDecoration(border: OutlineInputBorder()),
                    ),
                    SizedBox(height: 28),
                    TextFormField(
                      controller: descriController,
                      decoration: InputDecoration(border: OutlineInputBorder()),
                    ),
                    OutlineButton(
                      onPressed: () {
                        createNews(sharedBloc);
                      },
                      child: Text("Guardar Noticia"),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  createNews(MisNoticiasBloc bloc) {
    if (tituloController.text.isNotEmpty && descriController.text.isNotEmpty) {
      bloc.add(CrearNoticiaEvent(
          titulo: tituloController.text,
          descripcion: descriController.text,
          autor: "Sebastián",
          fuente: "Yooooo"));
    }
  }
}

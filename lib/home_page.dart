import 'package:flutter/material.dart';
import 'package:noticias/buscar/buscar.dart';
import 'package:noticias/mis_noticias/creadas/mis_noticias.dart';
import 'package:noticias/noticias/noticias.dart';
import 'package:noticias/nuevo/crear_noticia.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentPageIndex = 0;
  final _pagesList = [
    Noticias(),
    Buscar(),
    MisNoticias(),
    CrearNoticia(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _currentPageIndex,
        children: _pagesList,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentPageIndex,
        selectedItemColor: Colors.indigo,
        unselectedItemColor: Colors.grey,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        onTap: (value) {
          _currentPageIndex = value;
          setState(() {});
        },
        items: [
          BottomNavigationBarItem(
            label: 'Noticias',
            icon: Icon(
              Icons.article_rounded,
            ),
          ),
          BottomNavigationBarItem(
            label: 'Buscar',
            icon: Icon(
              Icons.search_rounded,
            ),
          ),
          BottomNavigationBarItem(
            label: 'Mis Noticias',
            icon: Icon(
              Icons.bookmark_rounded,
            ),
          ),
          BottomNavigationBarItem(
            label: 'Crear',
            icon: Icon(
              Icons.subject_rounded,
            ),
          ),
        ],
      ),
    );
  }
}

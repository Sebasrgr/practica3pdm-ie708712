import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:noticias/models/noticias.dart';
import 'package:path/path.dart' as Path;

part 'mis_noticias_event.dart';
part 'mis_noticias_state.dart';

class MisNoticiasBloc extends Bloc<MisNoticiasEvent, MisNoticiasState> {
  List<Noticia> _noticiasList;
  List<DocumentSnapshot> _documentsList;
  List<Noticia> get getNoticiasList => _noticiasList;
  File chosenImage;
  MisNoticiasBloc() : super(MisNoticiasInitial());

  @override
  Stream<MisNoticiasState> mapEventToState(
    MisNoticiasEvent event,
  ) async* {
    if (event is LeerNoticiasEvent) {
      try {
        await _getAllNoticias();
        yield NoticiasDescargadasState();
      } catch (e) {
        yield NoticiasErrorState(
            errorMessage: "No fue posible descargar las noticias.");
      }
    } else if (event is CargarImagenEvent) {
      chosenImage = await _chooseImage(event.takePictureFromCamera);
      yield ImagenCargadaState(imagen: chosenImage);
    } else if (event is CrearNoticiaEvent) {
      try {
        String imageUrl = await _uploadPicture(chosenImage);
        await _saveNoticia(event.titulo, event.descripcion, event.autor,
            event.fuente, imageUrl);
        yield NoticiaCreadaState();
      } catch (e) {
        yield NoticiasErrorState(errorMessage: "No se pudo guardar noticia.");
      }
    }
  }

  Future<File> _chooseImage(bool fromCamera) async {
    final picker = ImagePicker();
    final PickedFile chooseImage = await picker.getImage(
      source: fromCamera ? ImageSource.camera : ImageSource.gallery,
      maxHeight: 720,
      maxWidth: 720,
      imageQuality: 85,
    );
    return File(chooseImage.path);
  }

  Future _saveNoticia(
    String titulo,
    String descripcion,
    String autor,
    String fuente,
    String imageUrl,
  ) async {
    // Crea un doc en la collection de apuntes

    await FirebaseFirestore.instance.collection("noticias").doc().set({
      "titulo": titulo,
      "descripcion": descripcion,
      "autor": autor,
      "fuente": fuente,
      "imagen": imageUrl,
      "fecha": DateTime.now().toString(),
    });
  }

  Future _getAllNoticias() async {
    // recuperar lista de docs guardados en Cloud firestore
    // mapear a objeto de dart (Noticias)
    // agregar cada ojeto a una lista
    var misNoticias =
        await FirebaseFirestore.instance.collection("noticias").get();
    _documentsList = misNoticias.docs;
    _noticiasList = misNoticias.docs
        .map(
          (elemento) => Noticia(
            title: elemento["titulo"],
            author: elemento["autor"],
            description: elemento["descripcion"],
            urlToImage: elemento["imagen"],
            publishedAt: elemento["fecha"],
          ),
        )
        .toList();
  }

  Future<String> _uploadPicture(File image) async {
    String imagePath = image.path;
    // referencia al storage de firebase
    StorageReference reference = FirebaseStorage.instance
        .ref()
        .child("apuntes/${Path.basename(imagePath)}");

    // subir el archivo a firebase
    StorageUploadTask uploadTask = reference.putFile(image);
    await uploadTask.onComplete;

    // recuperar la url del archivo que acabamos de subir
    dynamic imageURL = await reference.getDownloadURL();
    return imageURL;
  }
}

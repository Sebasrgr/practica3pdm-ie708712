import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noticias/mis_noticias/bloc/mis_noticias_bloc.dart';
import 'package:noticias/noticias/item_noticia.dart';

class MisNoticias extends StatefulWidget {
  MisNoticias({Key key}) : super(key: key);

  @override
  _MisNoticiasState createState() => _MisNoticiasState();
}

class _MisNoticiasState extends State<MisNoticias> {
  MisNoticiasBloc _bloc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) {
          _bloc = MisNoticiasBloc();
          return _bloc..add(LeerNoticiasEvent());
        },
        child: BlocConsumer<MisNoticiasBloc, MisNoticiasState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            if (state is NoticiasDescargadasState) {
              return ListView.builder(
                itemCount: _bloc.getNoticiasList.length,
                itemBuilder: (BuildContext context, int index) {
                  return ItemNoticia(noticias: _bloc.getNoticiasList[index]);
                },
              );
            }
            return Center(
              child: Text('No se pudieron cargar las noticias.'),
            );
          },
        ),
      ),
    );
  }
}
